# crankin

## Build

*Step by step*

In each folder, `package` and `add-ons` and `examples` run:
```
yarn install
yarn run webpack
```
*All at once (shortcut)*

From the repo root:
```
cd package && yarn install && yarn run webpack && cd ../add-ons && yarn install && yarn run webpack && cd ../examples && yarn install && yarn run webpack && cd ..
```

## Running the Examples

### VS Code Debugging

If you're using VS Code, you can alter the default "Launch {BrowserName}" configuration by adding the following option: 

``` "file": "${workspaceFolder}/examples/src/", ```

Now when you hit F5, it should open to the parent directory in whatever browser you've chosen. 

Additionally, if you'd like your updates to sync, run ``` yarn start ``` ``