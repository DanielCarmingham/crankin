const path = require('path')

module.exports = {
    mode: 'development',
    entry: {
        dataFetching: './src/data-fetching/index.tsx',
        dataProvider: './src/data-provider/index.tsx',
        todos: './src/todos/index.tsx',
        todosWithClasses: './src/todos-with-classes/index.tsx',
        usingHooks: './src/using-hooks/index.tsx'
    },
    module: {
        rules: [
          {
            test: /\.(ts|tsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader', 'ts-loader']
          }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name]-compiled.js'
    },
    devtool: 'source-map'
}