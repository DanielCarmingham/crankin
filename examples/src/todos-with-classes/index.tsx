import { dispatch } from 'crankin';
import React from 'react';
import ReactDOM from 'react-dom';
import TodoStore, { TodoStoreState } from './TodoStore';
import { Button, Container, Checkbox, Grid, Paper, TextField } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

class TodoContainer extends React.Component<{}, TodoStoreState> {
    
    constructor(props: {}) {
        super(props); 

        this.state = TodoStore.getState();

        this.onChange = this.onChange.bind(this);
        this.remove = this.remove.bind(this);
        this.toggleComplete = this.toggleComplete.bind(this);
    }

    private callbackId?: number;

    componentDidMount() {
        this.callbackId = TodoStore.listen(this.onChange);
    }
    
    componentWillUnmount() {
        if (this.callbackId) 
            TodoStore.mute(this.callbackId);
    }

    onChange(state: TodoStoreState) { this.setState(state); }

    render()  {
        
        const styles = { 
            root: {
                flexGrow: 1,
                overflow: 'hidden',
                padding: '2rem'
            },
            paper: {
                maxWidth: 400,
                margin: '100px auto',
                padding: '2rem'
            }
        };
        return (
            <Container>
                <Paper style={styles.paper}>
                <Grid container direction="row" justifyContent="center" alignItems="center">
                    <TodoCreator/>
                </Grid>
                </Paper>
                <Grid item xs={12} md={6}>
                    <Typography variant="h6">
                        Todos 
                    </Typography>
                    <List>
                        {
                            this.state.todos.map((todo) => {
                                return (
                                    <ListItem key={todo.id}>
                                        <ListItemText>{todo.text}</ListItemText>
                                        <Checkbox checked={todo.isComplete} onChange={() => this.toggleComplete(todo.id)}/>
                                        <Button onClick={() => this.remove(todo.id)}>Remove</Button>
                                    </ListItem>
                                )
                            })
                        }
                    </List>
                </Grid>
            </Container>
        )
    }
    
    remove(todoId: number) { dispatch('remove', todoId); }

    toggleComplete(todoId: number)  { dispatch('toggleComplete', todoId); }
}


class TodoCreator extends React.Component {

    constructor(props: {}) {
        super(props); 
    }

    private newTodoRef = React.createRef<HTMLInputElement>();

    addTodo() {
        const ref = this.newTodoRef.current;

        if(ref && ref.value && ref.value.length > 0) 
            dispatch('add', ref.value);
    }

    clearAll() { dispatch('clearAll'); }

    render() {
        return (
            <div>
                <TextField label="New Todo" inputRef={this.newTodoRef} autoFocus={true}/>
                <Button color="primary" onClick={() => this.addTodo()}>Add</Button>
                <Button onClick={() => this.clearAll()}>Clear All</Button>
            </div>
        )
    }
}

ReactDOM.render(<TodoContainer/>, document.getElementById("mount"));