import { dispatch } from 'crankin';
import React,  { InputHTMLAttributes, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import TodoStore, { TodoStoreState } from './TodoStore';
import { Button, Container, Checkbox, Grid, Paper, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 3)
    },
    paper: {
        maxWidth: 400,
        margin: `${theme.spacing(1)}px auto}`,
        padding: theme.spacing(2)
    }
})); 

const TodoContainer = (props: Partial<TodoStoreState>) => {
    const classes = useStyles();
    const [todoState, setTodoState] = useState(TodoStore.getState());

    useEffect(() => {
        const callbackId = TodoStore.listen((nextState) => {
            setTodoState(nextState);
        });

        return () => TodoStore.mute(callbackId);
    }, []);

    const remove = (todoId: number): void => dispatch('remove', todoId);

    const toggleComplete = (todoId: number): void => dispatch('toggleComplete', todoId);

    return (
        <Container>
            <Paper className={classes.paper}>
            <Grid container direction="row" justifyContent="center" alignItems="center">
                <TodoCreator/>
            </Grid>
            </Paper>
            <Grid item xs={12} md={6}>
                <Typography variant="h6">
                    Todos 
                </Typography>
                <List>
                    {
                        todoState.todos.map((todo) => {
                            return (
                                <ListItem key={todo.id}>
                                    <ListItemText>{todo.text}</ListItemText>
                                    <Checkbox checked={todo.isComplete} onChange={() => toggleComplete(todo.id)}/>
                                    <Button onClick={() => remove(todo.id)}>Remove</Button>
                                </ListItem>
                            )
                        })
                    }
                </List>
            </Grid>
        </Container>
    )
}

const TodoCreator = () => {

    let newTodoRef = React.createRef<HTMLInputElement>();

    const addTodo = () => {
        const ref = newTodoRef.current;

        if(ref && ref.value && ref.value.length > 0) 
            dispatch('add', ref.value);
    }

    const clearAll = () => dispatch('clearAll');

    return (
        <div>
            <TextField label="New Todo" inputRef={newTodoRef} autoFocus={true}/>
            <Button color="primary" onClick={() => addTodo()}>Add</Button>
            <Button onClick={() => clearAll()}>Clear All</Button>
        </div>
    )
}

ReactDOM.render(<TodoContainer/>, document.getElementById("mount"));