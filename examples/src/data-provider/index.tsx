import React from 'react';
import ReactDOM from 'react-dom';
import UsersContainer from './UsersContainer'
import UsersStore from './UsersStore'
import { DataProvider } from 'crankin-add-ons';

const App = () => {
    return (
        <DataProvider store={UsersStore}>
            <UsersContainer/>
            <div>Test</div>
        </DataProvider>
    )
}

ReactDOM.render(<App/>, document.getElementById('mount'))
UsersStore.getAllUsers();