import React from 'react';
import { User } from './User';

const UserDetail = (props: { user: User }) => {
    if(!props.user)
        return <div>No User Selected</div>
    else
        return (
            <div>
                <h1>{props.user.name}</h1>
                <h4>{props.user.email}</h4>
                <h5>{props.user.website}</h5>
            </div>
        )
}

export default UserDetail;