import React from 'react';
import UserDetail from './UserDetail';
import { dispatch }from 'crankin';
import UsersStore, { UserStoreState } from './UsersStore';
import { User } from './User';

const UsersContainer = ({ users, selectedUser }: Partial<UserStoreState>) => {

    //TODO: need to figure out how to do this and retain type-safety: 
    const showUserDetail = (user: User) => UsersStore.showUserDetail(user);  // was: dispatch('showUserDetail', user);

    return (
        <div>
            {users && users.length > 0 ? users.map((user) => 
                <div key={user.id}>
                    <p onClick={() => showUserDetail(user)}>
                        {user.name}
                    </p>
                </div>
            ) : 'No Users'}
            {selectedUser ? <UserDetail user={selectedUser}/> : null}
        </div>
    )
}

export default UsersContainer;