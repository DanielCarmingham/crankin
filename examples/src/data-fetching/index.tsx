import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { dispatch } from 'crankin';
import UsersStore from './UsersStore';
import { User } from './User';

const UsersContainer = () => {

    const [usersState, setUsers] = useState(UsersStore.getState());

    useEffect(() => {
        const callbackid = UsersStore.listen(nextState => setUsers(nextState as any));
        return () => UsersStore.mute(callbackid);
    }, []);

    useEffect(() => {
        dispatch('getAllUsers')
    }, [])

    const showUserDetail = (user: User) => dispatch('showUserDetail', user);

    return (
        <div>
            {usersState.users.map((user) => 
                <div key={user.id}>
                    <p onClick={() => showUserDetail(user)}>
                        {user.name}
                    </p>
                </div>
            )}
            {usersState.selectedUser ? <UserDetail user={usersState.selectedUser}/> : null}
        </div>
    )
}

const UserDetail = (props: { user: User }) => {
    if(!props.user) 
        return <div>No User Selected or User Does Not Exist</div>
    else
        return (
            <div>
                <h1>{props.user.name}</h1>
                <h4>{props.user.email}</h4>
                <h5>{props.user.website}</h5>
            </div>
        )
}

ReactDOM.render(<UsersContainer/>, document.getElementById('mount'));