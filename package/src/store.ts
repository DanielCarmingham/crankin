const Stores: StoreMap = {};

export class Store<StateType> implements DynamicDispatchHandler {

    constructor(name: string) {
        this.state = {} as StateType;
        this.callbacks = {};
        
        Stores[name] = this;
    }
    
    protected state: StateType;

    private callbacks: any;

    private _callbackId = 0;

    static dispatch(type: string, ...args: any[]) {
        for (const key in Stores) {
            if (Object.prototype.hasOwnProperty.call(Stores, key)) {
                Stores[key].handle(type, args);
            }
        }
    }
    
    static createStore<NewStoreStateType>(name: string, store: StoreCreatorOptions<NewStoreStateType>): Store<NewStoreStateType> {
        const s = new Store<NewStoreStateType>(name);
        Object.assign(s.state, store.state || {});
        Object.assign(s, store.actions || {});
        return s;
    }
    
    emitChange() {
        for (const i in this.callbacks) {
            if (Object.prototype.hasOwnProperty.call(this.callbacks, i)) {
                this.callbacks[i]({...this.state});
            }
        }
    }

    getState() {
        return this.state;
    }

    isHandler(handlerName: string): handlerName is keyof typeof Store {
        return handlerName in this;
    }

    [k: string]: any;

    handle(type: string, args: any[]) {
        if (type in this) {
            const handler = this[type];
            if (handler instanceof Function)
                handler.apply(this, args);
        }
    }

    listen(callback: StoreListenCallback<StateType>): number {
        const id = this._callbackId++;
        this.callbacks[id] = callback;
        return id;
    }

    mute(id: number) {
        delete this.callbacks[id];
    }    
}

type HandlerFunc = (args: any[]) => void;

interface DynamicDispatchHandler {
    [key: string]: HandlerFunc;
}

type StoreMap = {
    [key: string]: Store<unknown>
}

export type StoreListenCallback<StateType> = (state: StateType) => void

export interface StoreCreatorOptions<StateType> {
    state?: StateType;
    actions?: any;
}

const createStore = Store.createStore;
const dispatch = Store.dispatch;

export { createStore, dispatch }

declare global {
    interface Window { Crankin: any; }
}

window.Crankin = Store;