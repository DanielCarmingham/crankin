const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
    entry: path.resolve(__dirname, 'react-add-ons/index.ts'),
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'ts-loader']
            }
        ]
    },
    externals: {
        react: 'react'
    },
    resolve: {
        extensions: ['.ts', '.tsx']
    },
    output: {
        library: 'crankin-add-ons',
        libraryTarget: 'umd',
        filename: 'crankin-add-ons.js'
    },
    devtool: "source-map",
    optimization: {
        minimizer: [new TerserPlugin({
            extractComments: false
        })]
    }
};